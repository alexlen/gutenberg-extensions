import { createHooks } from '@wordpress/hooks';

window.wp = wp || {};
wp.hooks = wp.hooks || createHooks();

// Our filter function to set custom classes
function setBlockCustomClassName( className, blockName ) {
    if (blockName === 'core/columns') {
		return className + ' row';
	} else if (blockName === 'core/column') {
		return 'col-md';
	} else {
		return className;
	}
}

// Adding the filter
wp.hooks.addFilter(
    'blocks.getBlockDefaultClassName',
    'my-plugin/set-block-custom-class-name',
    setBlockCustomClassName
);
<?php
/**
 * Plugin Name:       Test Plugin
 * Description:       Demo Test Plugin.
 * Version:           1.0.0
 * Author:            Adam Silverstein
 * Author URI:        http://10up.com
 * License:           GPLv2 or later
 * Text Domain:       Test Plugin
 * Domain Path:       /lang/
 * GitHub Plugin URI: https://github.com/adamsilverstein/test-plugin
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function gutenberg_extensions_assets() {
	wp_enqueue_script(
		'test-plugin',
		plugins_url( 'dist/main.js', __FILE__ ),
		array( 'jquery' ),
		false,
		true
	);
}

add_action( 'enqueue_block_assets', 'gutenberg_extensions_assets' );